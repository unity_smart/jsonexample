﻿using System;

using Newtonsoft.Json;

namespace JsonMaintenance
{
    public struct Step
    {
        public string PreviousStep;

        public string Text;
        public string ImageFile;
        public string VideoFile;

        public string NextStep;
    }

    public class DataHandler
    {
        DataHandler()
        {
            var step = new Step
            {
                PreviousStep = "prev",
                NextStep = "next",
                Text = "yeah, replace that bolt",
                ImageFile = "Bolt.jpg"
            };
            Console.WriteLine(step);

            var json = JsonConvert.SerializeObject(step, Formatting.Indented);
            Console.WriteLine(json);

            var sameStep = JsonConvert.DeserializeObject<Step>(json);
            Console.WriteLine(sameStep);
        }
    }
}